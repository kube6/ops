{{- define "nestjs.livenessProbe" -}}
failureThreshold: 3
httpGet:
  path: {{ hasKey .config "path" | ternary .config.path "/" }}
  port: {{ hasKey .config "port" | ternary .config.port 3000 }}
initialDelaySeconds: 10
periodSeconds: 10
successThreshold: 1
timeoutSeconds: 15
{{- end }}

{{- define "nestjs.readinessProbe" -}}
failureThreshold: 3
httpGet:
  path: {{ hasKey .config "path" | ternary .config.path "/" }}
  port: {{ hasKey .config "port" | ternary .config.port 3000 }}
initialDelaySeconds: 10
periodSeconds: 10
successThreshold: 3
timeoutSeconds: 15
{{- end }}
