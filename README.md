# Ops repository

This repository contains everything you need to know about the deployment of this whole group. The deployment is done with [Helm](https://helm.sh/).

**Quick access:**
* [Getting started](#getting-started)
* [Project structure](#project-structure)
    * [NestJs chart](#nestjs-chart)
    * [Observability chart](#observability-chart)
* [CI/CD](#cicd)

## Getting started

### Requirement 

* `Helm` version > 3.0.0
* Kubernetes cluster
* Admin access to the cluster

### Commands

The only command you will need to get started is [helm upgrade](https://helm.sh/docs/helm/helm_upgrade/). First release deployment needs to be the chart [observability](#observability-chart), run following command to deploy:

```
# Replace $NAMESPACE by the target namespace you want it to be installed
# Replace $RELEASE_NAME by the name of release
# If namespace doesn't exist it will create it
helm upgrade --install -n ${NAMESPACE} \ 
    --set registrySecret.enabled="true"  \ 
    --set registrySecret.registry="registry.gitlab.com"  \ 
    # See more about registry credentials there https://tinyurl.com/2rj8cx4b
    --set registrySecret.username="$USERNAME"  \
    --set registrySecret.password="$PASSWORD"  \ 
    ${RELEASE_NAME} charts/observability
```

After you've installed this chart, a new `ServiceAccount` named `app-sa` has been created. You can find the related secret to this SA by doing:
```
$/kubectl get secret --namespace=app
# Here is the expected result
NAME                  TYPE                                  DATA   AGE
app-sa-token-stn*l    kubernetes.io/service-account-token   3      5m57s # The ServiceAccount name is a variable we want
default-token-8zmlm   kubernetes.io/service-account-token   3      37m
```

```
# We use the previously found name to get the authenticiation token
kubectl get secret app-sa-token-stn*l --namespace=app -o yaml

# You will get something similar
apiVersion: v1
data:
  ca.crt: OMITTED
  namespace: YXBw
  token: OMITTED            # This field is what we expect, you have to copy this into your KUBECONFIG, remember it is base64 you need to decode it first
kind: Secret
metadata:
  annotations:
    kubernetes.io/service-account.name: app-sa
    kubernetes.io/service-account.uid: OMITTED
  creationTimestamp: "2021-07-16T07:01:14Z"
  name: app-sa-token-stn*l
  namespace: app
  resourceVersion: "14127130899"
  uid: OMITTED
type: kubernetes.io/service-account-token
```

These credentials are used as GitLab CI/CD Variables in this repository to deploy, these credentials cannot deploy `observability` chart.

Next, you can either wait for your next GitLab pipeline to deploy [`nestjs` chart](#nestjs-chart), or you can upgrade with the chart: 

```
# Replace $NAMESPACE by the target namespace you want it to be installed
# Replace $RELEASE_NAME by `nestjs` as it is the same as `.gitlab-ci.yml` 
# If namespace doesn't exist it will create it
helm upgrade --install -n ${NAMESPACE} ${RELEASE_NAME} charts/nestjs
```

After this, you should be ready to go, have fun and enjoy on `exam.do.local` domain, here is the list of web services available:

| Service Name       | Path                         | Description                              | 
| ------------------ | ---------------              | ---------------------------------------- |
| NestJS Application | GET exam.do.local/                        | Our main NestJS Application              | 
| Grafana            | GET exam.do.local/grafana                 | Grafana dashboard                        |
| Prometheus         | Get exam.do.local/prom/graph              | Prometheus server monitoring the cluster |  

## Nestjs chart

This chart is available at [charts/nestjs](charts/nestjs), and will upgrade versions of deployed [nestjs](https://gitlab.com/kube6/kube-tp) application. This chart is deployed
by [CI/CD](#cicd) everytime something does an update onto NestJS Application.

This chart create 3 resources:
- `Service` to expose the application as a ClusterIP
- `ConfigMap` to populate environment variables of application
- `Deployment` which contains configuration of containers to deploy

Pods of this chart is by default set to 2 replicas and is using rolling update with a max surge of 1 pod.

## Observability chart

This chart is about infrastructure deployment. It must be ran with all access to the cluster as it will create a lot of resources, be aware that this chart was made for a cluster hosted on [Scaleway](https://www.scaleway.com/en/), some part of this chart can differ depending on your provider.

Here is the list of resources created by this chart: 

- [Nginx Ingress Controller](https://kubernetes.github.io/ingress-nginx) (if `ingressnginx.deploy` is truthy)
- [Grafana](https://grafana.github.io/helm-charts) (if `grafana.deploy` is truthy)
- [Prometheus](https://prometheus-community.github.io/helm-charts) (if `prometheus.deploy` is truthy)
- Namespace if not already created
- ServiceAccount, Role and RoleBinding for GitLab automated deployment
- Ingress for NestJs Application
- Secret which is holding GitLab registry secrets

### Grafana

Grafana is created with a Prometheus datasource and multiple dashboards (see [values](charts/observability/values.yaml)):
- [Nginx Ingress Controller](https://grafana.com/grafana/dashboards/9614) dashboard
- Custom dashboard which use metrics exposed by NestJS Application

These two dashboards are in different folders, respectively `nginx-ingress` and `nodejs`. Here
is what the custom dashboard looks like (it is simple as the objective is to show that Prometheus is properly getting metrics from NestJS Application): 

![Grafana](assets/grafana.png)


Be aware that we did not hardcoded grafana password, it is available in `Secrets` as `{{ Release.Name }}-grafana`. Use the following command to get it :

```
# Replace ${NAMESPACE} by the target namespace where is grafana
kubectl get secret --namespace ${NAMESPACE} grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

### Prometheus

A complete prometheus server (with node-metrics...) is deployed in the cluster and monitors the pods, included NestJS Application. These metrics are used in Grafana dashboard.

If your prometheus and you NestJS application are running properly, you should be able to see the following thing at [`exam.do.local/prom/targets`](http://exam.do.local/prom/targets):

![Prometheus targets](assets/prometheus.png)

## CI/CD

In this CI, we don't check if the application is building properly, we expect it to be as it is triggered once its [own pipeline](https://gitlab.com/kube6/kube-tp) succeed. Below is an image of what the pipeline looks like, they will be explained.

![Pipeline](assets/pipeline.png)

```
Stage .pre:
    dont-interrupt-me       --- Prevent the pipeline from getting interrupted by more recent deployment & may lock the helm upgrade
    pull_repos              --- Dynamic job which clone a remote repository and makes it available for other jobs (see .gitlab-ci.yml)

Stage package:
    docker_build_nestjs     --- Build docker image of NestJS Application & push it with tag nestjs/latest and nestjs/${CI_COMMIT_SHA_SHORT}
    
Stage deploy:
    helm_deploy             --- Upgrade the release with the new container 
```

`pull_repos` is dynamic and is using [matrix](https://docs.gitlab.com/ee/ci/yaml/#parallel-matrix-trigger-jobs), so we can add as much repository as we want.


## License

This repository is [Apache2 Licensed](LICENSE).